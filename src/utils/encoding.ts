const base64Decode = (source: string): string =>
  Buffer.from(source, "base64").toString();

const base64Encode = (source: string): string =>
  Buffer.from(source).toString("base64");

export { base64Decode, base64Encode };
