import crypto from "crypto";

const blowfishDecrypt = (source: Buffer, key: Buffer): Buffer => {
  const decipher = crypto.createDecipheriv("BF-ECB", key, null);
  decipher.setAutoPadding(false);

  const blockSize = 8;
  const offset = source.length - (source.length % blockSize);
  const trimmed = source.slice(0, offset);

  return Buffer.concat([
    decipher.update(trimmed),
    decipher.final(),
    source.slice(offset, source.length),
  ]);
};

const blowfishEncrypt = (source: Buffer, key: Buffer): Buffer => {
  const cipher = crypto.createCipheriv("BF-ECB", key, null);

  return Buffer.concat([cipher.update(source), cipher.final()]);
};

export { blowfishDecrypt, blowfishEncrypt };
