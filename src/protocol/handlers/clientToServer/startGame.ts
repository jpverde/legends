import type { IENetPeer } from "enet-js";

import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { startGame } from "../../packets/structs/startGame";

export const handleStartGame = (peer: IENetPeer): void => {
  send(
    peer,
    Channels.serverToClient,
    startGame({
      command: Commands.startGameResponse,
      enablePause: 1,
    })
  );
};
