import { performance } from "perf_hooks";

import type { IENetPeer } from "enet-js";

import { Movements } from "../../../game/enums/movements";
import { store } from "../../../game/state/store";
import { serializeArray } from "../../../utils/serialization";
import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { movementRequest } from "../../packets/structs/movementRequest";
import { movementResponse } from "../../packets/structs/movementResponse";

export const handleMovement = (data: Buffer, peer: IENetPeer): void => {
  const { netId, type, x, z, targetNetId, bitField } = movementRequest(data);
  console.log(peer.address, targetNetId, "Moving to", { x, z });

  // Intentional usage
  // eslint-disable-next-line no-bitwise
  const waypointsLength = bitField >> 1;
  const { players } = store.getState();

  switch (type) {
    case Movements.move:
      players.forEach((player) => {
        if (player.peer) {
          send(
            player.peer,
            Channels.lowPriority,
            movementResponse({
              command: Commands.movementResponse,
              count: waypointsLength,
              netId,
              syncId: performance.now(),
              // Sending as received, pending to process properly
              // eslint-disable-next-line @typescript-eslint/no-magic-numbers
              unknown: serializeArray(Array.from(data.slice(18))),
            })
          );
        }
      });
      break;

    default:
      console.error("Unhandled movement type", type);
      break;
  }
};
