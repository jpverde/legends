import type { IENetPeer } from "enet-js";

import { Teams } from "../../../game/enums/teams";
import { store } from "../../../game/state/store";
import { serializeString } from "../../../utils/serialization";
import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { characterSpawn } from "../../packets/structs/characterSpawn";
import { endSpawn } from "../../packets/structs/endSpawn";
import { enterVisibilityClient } from "../../packets/structs/enterVisiblityClient";
import { playerInfo } from "../../packets/structs/playerInfo";
import { startSpawn } from "../../packets/structs/startSpawn";

const getTeamNumber = (teamId: Teams): number => {
  if (teamId === Teams.blue) {
    return 1;
  }

  return 0;
};

const spawnCharacters = (peer: IENetPeer): void => {
  const { players } = store.getState();

  players
    .filter((player) => player.clientId > -1)
    .forEach((player) => {
      send(
        peer,
        Channels.serverToClient,
        characterSpawn({
          characterName: serializeString(player.character.name),
          clientId: player.clientId,
          command: Commands.characterSpawn,
          netId: player.character.netId,
          // Intentional duplicate
          netId2: player.character.netId,
          netNodeId: 40,
          playerName: serializeString(player.name),
          skinId: player.character.skin,
          teamNumber: getTeamNumber(player.teamId),
        })
      );

      send(
        peer,
        Channels.serverToClient,
        playerInfo({
          command: Commands.playerInfo,
          level: player.level,
          netId: player.character.netId,
          spell1: player.spell1,
          spell2: player.spell2,
          wardSkin: player.wardSkin,
        })
      );

      send(
        peer,
        Channels.serverToClient,
        enterVisibilityClient({
          command: Commands.enterVisibilityClient,
          netId: player.character.netId,
        })
      );
    });
};

export const handleCharacterLoaded = (peer: IENetPeer): void => {
  send(
    peer,
    Channels.serverToClient,
    startSpawn({ command: Commands.startSpawn })
  );

  spawnCharacters(peer);

  send(peer, Channels.serverToClient, endSpawn({ command: Commands.endSpawn }));
};
