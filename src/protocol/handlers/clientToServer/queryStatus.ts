import type { IENetPeer } from "enet-js";

import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { queryStatusResponse } from "../../packets/structs/queryStatusResponse";

export const handleQueryStatus = (peer: IENetPeer): void => {
  const response = queryStatusResponse({
    command: Commands.queryStatusResponse,
    gameStatus: 1,
  });

  send(peer, Channels.serverToClient, response);
};
