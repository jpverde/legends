import type { IENetPeer } from "enet-js";

import { store } from "../../../game/state/store";
import { serializeArray, serializeString } from "../../../utils/serialization";
import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { syncVersionRequest } from "../../packets/structs/syncVersionRequest";
import { syncVersionResponse } from "../../packets/structs/syncVersionResponse";

export const handleSyncVersion = (data: Buffer, peer: IENetPeer): void => {
  const { netId, version } = syncVersionRequest(data);

  const versionBuffer = (version as { buffer: Buffer }).buffer;
  const clientVersion = versionBuffer.toString(
    "utf-8",
    0,
    versionBuffer.indexOf("\0")
  );
  const serverVersion = "Version 4.20.0.315 [PUBLIC]";

  const { match, players } = store.getState();

  send(
    peer,
    Channels.serverToClient,
    syncVersionResponse({
      command: Commands.syncVersionResponse,
      // Unknown at the moment, seems to be the result of bitwise operations
      gameFeatures: 487826,
      gameMode: serializeString(match.gameMode),
      hasValidVersion: clientVersion === serverVersion,
      mapId: match.map,
      netId,
      players: serializeArray(
        players.map((player) => ({
          icon: player.icon,
          id: player.playerId,
          level: player.level,
          ribbon: player.ribbon,
          spell1: player.spell1,
          spell2: player.spell2,
          teamId: player.teamId,
          tier: serializeString(player.tier),
        }))
      ),
      region: serializeString(match.region),
      version: serializeString(serverVersion),
    })
  );
};
