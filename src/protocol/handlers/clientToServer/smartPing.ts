import type { IENetPeer } from "enet-js";

import { SmartPings } from "../../../game/enums/smartPings";
import { store } from "../../../game/state/store";
import { getPlayerByPeer } from "../../../game/state/utils";
import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { smartPingRequest } from "../../packets/structs/smartPingRequest";
import { smartPingResponse } from "../../packets/structs/smartPingResponse";

export const handleSmartPing = (data: Buffer, peer: IENetPeer): void => {
  const { netId, x, y, targetNetId, type } = smartPingRequest(data);
  console.log("PING", {
    netId,
    targetNetId,
    type: SmartPings[type],
    x,
    y,
  });

  const peerPlayer = getPlayerByPeer(peer);

  if (peerPlayer) {
    const { character, teamId } = peerPlayer;
    const { players } = store.getState();

    players
      .filter((player) => player.teamId === teamId)
      .forEach((player) => {
        if (player.peer) {
          send(
            player.peer,
            Channels.serverToClient,
            smartPingResponse({
              command: Commands.smartPingResponse,
              // Unknown at the moment, seems to be the result of bitwise operations
              features: 251,
              netId,
              sourceNetId: character.netId,
              targetNetId,
              type,
              x,
              y,
            })
          );
        }
      });
  }
};
