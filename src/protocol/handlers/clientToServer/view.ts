import type { IENetPeer } from "enet-js";

import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { viewRequest } from "../../packets/structs/viewRequest";
import { viewResponse } from "../../packets/structs/viewResponse";

export const handleView = (data: Buffer, peer: IENetPeer): void => {
  const { syncId } = viewRequest(data);

  send(
    peer,
    Channels.serverToClient,
    viewResponse({ command: Commands.viewResponse, syncId })
  );
};
