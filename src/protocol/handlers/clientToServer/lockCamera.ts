import { lockCamera } from "../../packets/structs/lockCamera";

export const handleLockCamera = (data: Buffer): void => {
  const { bitfield, clientId } = lockCamera(data);

  // Intentional usage
  // eslint-disable-next-line no-bitwise
  const locked = (bitfield & 1) !== 0;

  if (locked) {
    console.log(clientId, "has locked their camera");
  } else {
    console.log(clientId, "has unlocked their camera");
  }
};
