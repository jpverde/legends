import type { IENetPeer } from "enet-js";

import { disconnect } from "../../../game/logic/player";

export const handleExit = (peer: IENetPeer): void => {
  disconnect(peer);
};
