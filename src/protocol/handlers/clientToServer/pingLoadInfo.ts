import type { IENetHost, IENetPeer } from "enet-js";

import { getPlayerByPeer } from "../../../game/state/utils";
import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { broadcast } from "../../packets";
import { pingLoadInfoRequest } from "../../packets/structs/pingLoadInfoRequest";
import { pingLoadInfoResponse } from "../../packets/structs/pingLoadInfoResponse";

export const handlePingLoadInfo = (
  data: Buffer,
  host: IENetHost,
  peer: IENetPeer
): void => {
  const { netId, loaded, unk1, ping, unk2, unk3 } = pingLoadInfoRequest(data);
  const peerPlayer = getPlayerByPeer(peer);

  if (peerPlayer) {
    const { clientId, playerId } = peerPlayer;

    broadcast(
      Channels.lowPriority,
      pingLoadInfoResponse({
        clientId,
        command: Commands.pingLoadInfoResponse,
        loaded,
        netId,
        ping,
        playerId,
        unk1,
        unk2,
        unk3,
      }),
      host
    );
  }
};
