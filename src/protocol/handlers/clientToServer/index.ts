import type { IENetHost, IENetPacket, IENetPeer } from "enet-js";

import { handleCharacterEmote } from "./characterEmote";
import { handleCharacterLoaded } from "./characterLoaded";
import { handleExit } from "./exit";
import { handleLockCamera } from "./lockCamera";
import { handleMovement } from "./movement";
import { handlePingLoadInfo } from "./pingLoadInfo";
import { handleQueryStatus } from "./queryStatus";
import { handleSmartPing } from "./smartPing";
import { handleStartGame } from "./startGame";
import { handleSyncVersion } from "./syncVersion";
import { handleView } from "./view";

import { Commands } from "../../enums/commands";
import { getData } from "../../packets";

// eslint-disable-next-line max-lines-per-function
export const handleClientToServer = (
  host: IENetHost,
  packet: IENetPacket,
  peer: IENetPeer
): void => {
  const data = getData(packet);
  const command = data.readUInt8(0);

  if (Commands[command]) {
    console.log(">> [CLIENTTOSERVER]", { command: Commands[command] });
  }

  switch (command) {
    case Commands.characterEmoteRequest:
      handleCharacterEmote(data);
      break;

    case Commands.characterLoaded:
      handleCharacterLoaded(peer);
      break;

    case Commands.exit:
      handleExit(peer);
      break;

    case Commands.lockCamera:
      handleLockCamera(data);
      break;

    case Commands.movementConfirm:
      break;

    case Commands.movementRequest:
      handleMovement(data, peer);
      break;

    case Commands.pingLoadInfoRequest:
      handlePingLoadInfo(data, host, peer);
      break;

    case Commands.queryStatusRequest:
      handleQueryStatus(peer);
      break;

    case Commands.smartPingRequest:
      handleSmartPing(data, peer);
      break;

    case Commands.startGameRequest:
      handleStartGame(peer);
      break;

    case Commands.syncVersionRequest:
      handleSyncVersion(data, peer);
      break;

    case Commands.viewRequest:
      handleView(data, peer);
      break;

    default:
      console.error(">> [CLIENTTOSERVER] Unhandled command:", command);
      break;
  }
};
