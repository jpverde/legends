import { store } from "../../../game/state/store";
import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { characterEmote } from "../../packets/structs/characterEmote";

export const handleCharacterEmote = (data: Buffer): void => {
  const { netId, emoteId } = characterEmote(data);
  const { players } = store.getState();

  // Pending to filter according to vision once implemented
  players.forEach((player) => {
    if (player.peer) {
      send(
        player.peer,
        Channels.serverToClient,
        characterEmote({
          command: Commands.characterEmoteResponse,
          emoteId,
          netId,
        })
      );
    }
  });
};
