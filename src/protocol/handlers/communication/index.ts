import type { IENetPacket, IENetPeer } from "enet-js";

import { handleChatMessage } from "./chatMessage";

import { Commands } from "../../enums/commands";
import { getData } from "../../packets";

export const handleCommunication = (
  packet: IENetPacket,
  peer: IENetPeer
): void => {
  const data = getData(packet);
  const command = data.readUInt8(0);

  console.log(
    ">> [COMMUNICATION]",
    { command: Commands[command] },
    peer.address
  );

  switch (command) {
    case Commands.chatMessage:
      handleChatMessage(data, peer);
      break;

    default:
      console.error(">> [COMMUNICATION] Unhandled command:", command);
      break;
  }
};
