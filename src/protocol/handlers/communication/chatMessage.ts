import type { IENetPeer } from "enet-js";

import { ChatChannels } from "../../../game/enums/chatChannels";
import { store } from "../../../game/state/store";
import { getPlayerByPeer } from "../../../game/state/utils";
import { Channels } from "../../enums/channels";
import { send } from "../../packets";
import { chatMessage } from "../../packets/structs/chatMessage";

export const handleChatMessage = (data: Buffer, peer: IENetPeer): void => {
  const { channel } = chatMessage(data);
  const peerPlayer = getPlayerByPeer(peer);

  if (peerPlayer) {
    const { name, teamId } = peerPlayer;

    const text = data.slice(chatMessage.size);
    console.log("MSG:", {
      channel: ChatChannels[channel],
      player: name,
      text: text.toString("utf-8", 0, text.indexOf("\0")),
    });

    const { players } = store.getState();

    switch (channel) {
      case ChatChannels.all: {
        players.forEach((player) => {
          if (player.peer) {
            send(player.peer, Channels.communication, chatMessage(data));
          }
        });
        break;
      }

      case ChatChannels.team:
        players
          .filter((player) => player.teamId === teamId)
          .forEach((player) => {
            if (player.peer) {
              send(player.peer, Channels.communication, chatMessage(data));
            }
          });

        break;

      default:
        console.error("Unhandled chat channel:", channel);
        break;
    }
  }
};
