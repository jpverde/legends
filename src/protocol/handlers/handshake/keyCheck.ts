import type { IENetPeer } from "enet-js";

import { connect } from "../../../game/logic/player";
import { keyCheckRequest } from "../../packets/structs/keyCheckRequest";

export const handleKeyCheck = (data: Buffer, peer: IENetPeer): void => {
  const { partialKey, playerId } = keyCheckRequest(data);

  connect(peer, playerId as number, partialKey.toArray());
};
