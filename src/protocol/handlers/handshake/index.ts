import type { IENetPacket, IENetPeer } from "enet-js";

import { handleKeyCheck } from "./keyCheck";

import { Commands } from "../../enums/commands";

export const handleHandshake = (packet: IENetPacket, peer: IENetPeer): void => {
  const { data } = packet;
  const command = data.readUInt8(0);

  console.log(">> [HANDSHAKE]", { command: Commands[command] });

  switch (command) {
    case Commands.keyCheck:
      handleKeyCheck(data, peer);
      break;

    default:
      console.error(">> [HANDSHAKE] Unhandled command:", command);
      break;
  }
};
