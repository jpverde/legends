import type { IENetPeer } from "enet-js";

import { Teams } from "../../../game/enums/teams";
import { store } from "../../../game/state/store";
import { serializeArray, serializeString } from "../../../utils/serialization";
import { Channels } from "../../enums/channels";
import { Commands } from "../../enums/commands";
import { send } from "../../packets";
import { loadScreenInfo } from "../../packets/structs/loadScreenInfo";
import { loadScreenPlayerCharacter } from "../../packets/structs/loadScreenPlayerCharacter";
import { loadScreenPlayerName } from "../../packets/structs/loadScreenPlayerName";

export const handleClientReady = (peer: IENetPeer): void => {
  const maxPlayers = 12;
  const { players } = store.getState();
  const blueTeamIds = players
    .filter((player) => player.teamId === Teams.blue)
    .map((player) => player.playerId);
  const redTeamIds = players
    .filter((player) => player.teamId === Teams.red)
    .map((player) => player.playerId);

  send(
    peer,
    Channels.loadingScreen,
    loadScreenInfo({
      blueIds: serializeArray(blueTeamIds),
      command: Commands.loadScreenInfo,
      maxPlayersBlue: maxPlayers - redTeamIds.length,
      maxPlayersRed: maxPlayers - blueTeamIds.length,
      redIds: serializeArray(redTeamIds),
      totalPlayersBlue: blueTeamIds.length,
      totalPlayersRed: redTeamIds.length,
    })
  );

  players.forEach((player) => {
    send(
      peer,
      Channels.loadingScreen,
      loadScreenPlayerName({
        command: Commands.loadName,
        length: player.name.length + 1,
        name: serializeString(player.name),
        playerId: player.playerId,
      })
    );
    send(
      peer,
      Channels.loadingScreen,
      loadScreenPlayerCharacter({
        command: Commands.loadChampion,
        length: player.character.name.length + 1,
        name: serializeString(player.character.name),
        playerId: player.playerId,
        skinId: player.character.skin,
      })
    );
  });
};
