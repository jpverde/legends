import type { IENetPacket, IENetPeer } from "enet-js";

import { handleClientReady } from "./clientReady";

import { Commands } from "../../enums/commands";
import { getData } from "../../packets";

export const handleLoadingScreen = (
  packet: IENetPacket,
  peer: IENetPeer
): void => {
  const data = getData(packet);
  const command = data.readUInt8(0);

  console.log(">> [LOADINGSCREEN]", { command: Commands[command] });

  switch (command) {
    case Commands.clientReady:
      handleClientReady(peer);
      break;

    default:
      console.error(">> [LOADINGSCREEN] Unhandled command:", command);
      break;
  }
};
