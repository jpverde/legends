import type { IENetHost, IENetPacket, IENetPeer } from "enet-js";

import { handleClientToServer } from "./clientToServer";
import { handleCommunication } from "./communication";
import { handleHandshake } from "./handshake";
import { handleLoadingScreen } from "./loadingScreen";

import { Channels } from "../enums/channels";

export const handle = (
  channel: Channels,
  host: IENetHost,
  packet: IENetPacket,
  peer: IENetPeer
): void => {
  switch (channel) {
    case Channels.handshake:
      handleHandshake(packet, peer);
      break;

    case Channels.clientToServer:
      handleClientToServer(host, packet, peer);
      break;

    case Channels.communication:
      handleCommunication(packet, peer);
      break;

    case Channels.loadingScreen:
      handleLoadingScreen(packet, peer);
      break;

    default:
      console.error("Packet received from unhandled channel", channel);
      break;
  }
};
