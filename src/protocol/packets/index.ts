import { enet } from "enet-js";
import type { IENetHost, IENetPacket, IENetPeer } from "enet-js";
import type { StructObjectBase } from "ref-struct-di";

import { Channels } from "../enums/channels";
import { Commands } from "../enums/commands";
import { blowfishDecrypt, blowfishEncrypt } from "../security/blowfish";

const minimumEncryptedLength = 8;
const encryptionKey = Buffer.from("17BLOhi6KZsTtldTsizvHg==", "base64");

const createPacket = (data: Buffer): IENetPacket | null => {
  const needsEncryption = data.length >= minimumEncryptedLength;

  if (needsEncryption) {
    const encryptedData = blowfishEncrypt(data, encryptionKey);

    return enet.packet.create(encryptedData);
  }

  return enet.packet.create(data);
};

const send = (
  peer: IENetPeer,
  channel: Channels,
  data: StructObjectBase
): void => {
  const serialized = data.ref();
  console.log(`<< [${Channels[channel].toUpperCase()}]`, {
    command: Commands[serialized.readUInt8(0)],
  });
  const packet = createPacket(serialized);

  if (packet === null) {
    console.error("Unable to create packet", {
      channel,
      data: serialized.toString("hex"),
    });
  } else {
    enet.peer.send(peer, channel, packet);
  }
};

const broadcast = (
  channel: Channels,
  data: StructObjectBase,
  host: IENetHost
): void => {
  const serialized = data.ref();
  console.log(`(( [${Channels[channel].toUpperCase()}]`, {
    command: Commands[serialized.readUInt8(0)],
  });
  const packet = createPacket(serialized);

  if (packet === null) {
    console.error("Unable to create packet", {
      channel,
      data: serialized.toString("hex"),
    });
  } else {
    enet.host.broadcast(host, channel, packet);
  }
};

const getData = (packet: IENetPacket): Buffer => {
  const isEncrypted = packet.dataLength >= minimumEncryptedLength;

  if (isEncrypted) {
    return blowfishDecrypt(packet.data, encryptionKey);
  }

  return packet.data;
};

export { broadcast, getData, send };
