import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const mastery = structType(
  [
    [ref.types.uint, "id"],
    [ref.types.byte, "level"],
  ],
  { packed: true }
);

const runesLength = 30;
const masteriesLength = 80;
export const playerInfo = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [arrayType(ref.types.uint, runesLength), "runes"],
    [ref.types.uint, "spell1"],
    [ref.types.uint, "spell2"],
    [arrayType(mastery, masteriesLength), "masteries"],
    [ref.types.byte, "level"],
    [ref.types.byte, "wardSkin"],
  ],
  { packed: true }
);
