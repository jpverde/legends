import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const summonerNameLength = 16;
export const loadScreenPlayerName = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.int64, "playerId"],
    [ref.types.int, "unk1"],
    [ref.types.int, "length"],
    [arrayType(ref.types.char, summonerNameLength), "name"],
    [ref.types.byte, "unk2"],
  ],
  { packed: true }
);
