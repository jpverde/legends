import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const playerUnk1Length = 128;
const tierLength = 24;
const playerStruct = structType(
  [
    [ref.types.int64, "id"],
    [ref.types.short, "level"],
    [ref.types.uint, "spell1"],
    [ref.types.uint, "spell2"],
    [ref.types.bool, "isBot"],
    [ref.types.int, "teamId"],
    [arrayType(ref.types.byte, playerUnk1Length), "unk1"],
    [arrayType(ref.types.char, tierLength), "tier"],
    [ref.types.int, "icon"],
    [ref.types.short, "ribbon"],
  ],
  { packed: true }
);

const playersLength = 12;
const versionLength = 256;
const gameModeLength = 128;
const regionLength = 2336;
const syncUnk1Length = 260;
export const syncVersionResponse = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.int, "netId"],
    [ref.types.bool, "hasValidVersion"],
    [ref.types.int, "mapId"],
    [arrayType(playerStruct, playersLength), "players"],
    [arrayType(ref.types.char, versionLength), "version"],
    [arrayType(ref.types.char, gameModeLength), "gameMode"],
    [arrayType(ref.types.char, regionLength), "region"],
    [ref.types.int, "gameFeatures"],
    [arrayType(ref.types.byte, syncUnk1Length), "unk1"],
  ],
  { packed: true }
);
