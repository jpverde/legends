import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const idsLength = 24;
export const loadScreenInfo = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.int, "maxPlayersBlue"],
    [ref.types.int, "maxPlayersRed"],
    [arrayType(ref.types.int64, idsLength), "blueIds"],
    [arrayType(ref.types.int64, idsLength), "redIds"],
    [ref.types.int, "totalPlayersBlue"],
    [ref.types.int, "totalPlayersRed"],
  ],
  { packed: true }
);
