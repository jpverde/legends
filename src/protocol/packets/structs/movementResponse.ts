import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const unknownLength = 32;

export const movementResponse = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.int, "syncId"],
    [ref.types.short, "count"],
    [arrayType(ref.types.byte, unknownLength), "unknown"],
  ],
  { packed: true }
);
