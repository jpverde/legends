import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const partialKeyLength = 3;
export const keyCheckResponse = structType(
  [
    [ref.types.byte, "command"],
    [arrayType(ref.types.byte, partialKeyLength), "partialKey"],
    [ref.types.int, "clientId"],
    [ref.types.int64, "playerId"],
    [ref.types.int, "unk1"],
    [ref.types.int64, "unk2"],
    [ref.types.int, "unk3"],
  ],
  { packed: true }
);
