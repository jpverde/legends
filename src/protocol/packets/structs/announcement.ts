import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const announcement = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.byte, "messageId"],
  ],
  { packed: true }
);
