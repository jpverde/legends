import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const pingLoadInfoRequest = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.int, "clientId"],
    [ref.types.int64, "playerId"],
    [ref.types.float, "loaded"],
    [ref.types.float, "unk1"],
    [ref.types.short, "ping"],
    [ref.types.short, "unk2"],
    [ref.types.byte, "unk3"],
  ],
  { packed: true }
);
