import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const smartPingResponse = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.float, "x"],
    [ref.types.float, "y"],
    [ref.types.uint, "targetNetId"],
    [ref.types.uint, "sourceNetId"],
    [ref.types.byte, "type"],
    [ref.types.byte, "features"],
  ],
  { packed: true }
);
