import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const movementRequest = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.byte, "type"],
    [ref.types.float, "x"],
    [ref.types.float, "z"],
    [ref.types.uint, "targetNetId"],
    [ref.types.byte, "bitField"],
  ],
  { packed: true }
);
