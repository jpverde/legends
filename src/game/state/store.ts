import { configureStore } from "@reduxjs/toolkit";

import { matchState } from "./match";
import { playersState } from "./players";

const store = configureStore({
  devTools: false,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),
  reducer: {
    match: matchState.reducer,
    players: playersState.reducer,
  },
});

export { store };
