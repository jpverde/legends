import { createSlice } from "@reduxjs/toolkit";

import type { IMatchState } from "./types";

import { Maps } from "../enums/maps";
import { Regions } from "../enums/regions";

const initialState: IMatchState = {
  gameMode: "CLASSIC",
  map: Maps.howlingAbyss,
  nextNetId: 1073741836,
  region: Regions.lan,
};

export const matchState = createSlice({
  initialState,
  name: "match",
  reducers: {},
});
