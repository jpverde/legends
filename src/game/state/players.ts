import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import type { IENetPeer } from "enet-js";

import type { IPlayerState } from "./types";

import { characters } from "../enums/characters";
import { Spells } from "../enums/spells";
import { Teams } from "../enums/teams";
import { Tiers } from "../enums/tiers";

const initialState: IPlayerState[] = [
  {
    character: {
      name: characters.theVoidWalker.name,
      netId: 1073741824,
      skin: characters.theVoidWalker.skins.harbinger,
    },
    clientId: 0,
    icon: 659,
    level: 30,
    name: "Player 1",
    peer: null,
    playerId: 1,
    ribbon: 2,
    spell1: Spells.ignite,
    spell2: Spells.flash,
    teamId: Teams.blue,
    tier: Tiers.diamond,
    wardSkin: 0,
  },
];

export const playersState = createSlice({
  initialState,
  name: "players",
  reducers: {
    connect: (
      players,
      action: PayloadAction<{ peer: IENetPeer; playerId: number }>
    ) =>
      players.map((player) => {
        if (player.playerId === action.payload.playerId) {
          return { ...player, peer: action.payload.peer };
        }

        return player;
      }),
    disconnect: (players, action: PayloadAction<{ playerId: number }>) =>
      players.map((player) => {
        if (player.playerId === action.payload.playerId) {
          return { ...player, peer: null };
        }

        return player;
      }),
  },
});
