import type { IENetPeer } from "enet-js";

import type { Maps } from "../enums/maps";
import type { Regions } from "../enums/regions";
import type { Spells } from "../enums/spells";
import type { Teams } from "../enums/teams";
import type { Tiers } from "../enums/tiers";

interface ICharacterState {
  name: string;
  netId: number;
  skin: number;
}

interface IPlayerState {
  character: ICharacterState;
  clientId: number;
  icon: number;
  level: number;
  name: string;
  peer: IENetPeer | null;
  playerId: number;
  ribbon: number;
  spell1: Spells;
  spell2: Spells;
  teamId: Teams;
  tier: Tiers;
  wardSkin: number;
}

interface IMatchState {
  gameMode: string;
  map: Maps;
  nextNetId: number;
  region: Regions;
}

export type { IMatchState, IPlayerState };
