import type { IENetPeer } from "enet-js";

import { store } from "./store";
import type { IPlayerState } from "./types";

const getPlayerByPeer = (peer: IENetPeer): IPlayerState | undefined =>
  store
    .getState()
    .players.find(
      (player) =>
        player.peer &&
        player.peer.address.host === peer.address.host &&
        player.peer.address.port === peer.address.port
    );

export { getPlayerByPeer };
