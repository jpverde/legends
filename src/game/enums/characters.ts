import { base64Decode } from "../../utils/encoding";

interface ICharacter {
  name: string;
  skins: Record<string, number>;
}

export const characters: Record<string, ICharacter> = {
  theVoidWalker: {
    name: base64Decode("S2Fzc2FkaW4="),
    skins: {
      deepOne: 2,
      festival: 1,
      harbinger: 4,
      original: 0,
      preVoid: 3,
    },
  },
};
