export enum Tiers {
  bronze = "BRONZE",
  challenger = "CHALLENGER",
  diamond = "DIAMOND",
  gold = "GOLD",
  master = "MASTER",
  platinum = "PLATINUM",
  silver = "SILVER",
}
