export enum Maps {
  classicSummonersRift = 1,
  crystalScar = 8,
  howlingAbyss = 12,
  modernSummonersRift = 11,
  twistedTreeline = 10,
}
