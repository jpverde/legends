import type { IENetPeer } from "enet-js";

import { Channels } from "../../protocol/enums/channels";
import { Commands } from "../../protocol/enums/commands";
import { send } from "../../protocol/packets";
import { announcement } from "../../protocol/packets/structs/announcement";
import { keyCheckResponse } from "../../protocol/packets/structs/keyCheckResponse";
import { serializeArray } from "../../utils/serialization";
import { Announcements } from "../enums/announcements";
import { playersState } from "../state/players";
import { store } from "../state/store";
import { getPlayerByPeer } from "../state/utils";

const connect = (
  peer: IENetPeer,
  playerId: number,
  partialKey: number[]
): void => {
  store.dispatch(playersState.actions.connect({ peer, playerId }));

  const { players } = store.getState();
  players.forEach((player) => {
    send(
      peer,
      Channels.handshake,
      keyCheckResponse({
        clientId: player.clientId,
        command: Commands.keyCheck,
        partialKey: serializeArray(partialKey),
        playerId: player.playerId,
      })
    );
  });
};

const disconnect = (peer: IENetPeer): void => {
  const peerPlayer = getPlayerByPeer(peer);

  if (peerPlayer) {
    const { playerId, teamId, character } = peerPlayer;
    store.dispatch(playersState.actions.disconnect({ playerId }));

    const { players } = store.getState();
    players
      .filter((player) => player.teamId === teamId)
      .forEach((player) => {
        if (player.peer) {
          send(
            player.peer,
            Channels.serverToClient,
            announcement({
              command: Commands.announcement,
              messageId: Announcements.playerDisconnected,
              netId: character.netId,
            })
          );
        }
      });
  }
};

export { connect, disconnect };
