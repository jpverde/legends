import type { IENetAddress, IENetEvent, IENetHost } from "enet-js";
import { ENET_HOST_ANY, ENetEventType, enet } from "enet-js";

import { disconnect } from "./game/logic/player";
import { handle } from "./protocol/handlers";

const PEER_MTU = 996;

const update = (host: IENetHost): void => {
  const event: IENetEvent | null = enet.host.service(host, 0);

  if (event) {
    switch (event.type) {
      case ENetEventType.none:
        break;

      case ENetEventType.connect:
        // eslint-disable-next-line fp/no-mutation
        event.peer.mtu = PEER_MTU;
        console.log(
          "connected",
          event.peer.address.host,
          event.peer.address.port
        );
        break;

      case ENetEventType.disconnect:
        console.log(
          "disconnected",
          event.peer.address.host,
          event.peer.address.port
        );
        disconnect(event.peer);
        break;

      case ENetEventType.receive:
        handle(event.channelID, host, event.packet, event.peer);
        enet.packet.destroy(event.packet);
        break;

      default:
        console.error("Unhandled event", event);
        break;
    }
  }
};

const ADDRESS: IENetAddress = { host: ENET_HOST_ANY, port: 5000 };
const MAX_PEERS = 32;

const start = (): void => {
  if (enet.initialize() === 0) {
    const host: IENetHost | null = enet.host.create(ADDRESS, MAX_PEERS, 0, 0);

    if (host === null) {
      console.error("Unable to create host");
    } else {
      console.log("Server running on port", ADDRESS.port);
      setInterval(() => {
        update(host);
      });
      process.on("SIGINT", (): void => {
        enet.host.destroy(host);
        enet.deinitialize();
        process.exit();
      });
    }
  } else {
    console.error("Unable to initialize ENet");
  }
};

start();
